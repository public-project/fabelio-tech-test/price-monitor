# Overview
This web application for price monitor

# Prerequisite
* Nodejs
* Vuejs
* Vuetify

# Build and Run
Run this command on terminal
```
npm run serve
```
It will start the application

# URLs
Browse on chrome, opera or other browser
```
http://localhost:8080
```